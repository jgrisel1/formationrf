*** Settings ***
Resource    squash_resources.resource
Library    SeleniumLibrary

*** Keywords ***
Test Setup
	Set Selenium Implicit Wait    2 seconds
    Open Browser    ${url}    ${browser}
    Maximize Browser Window

Test Teardown
	Close Browser

*** Test Cases ***
Connexion test
	[Setup]	Test Setup
	Given L'utilisateur est sur la page d'accueil
	When L'utilisateur clique sur le bouton make appointment
	Then La page de connexion s'affiche
	When L'utilisateur se connecte avec un login et mot de passe
	Then L'utilisateur est connecté sur la page de rendez-vous
	[Teardown]	Test Teardown