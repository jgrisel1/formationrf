package gherkinJava;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class KatalonSteps {
	
	WebDriver driver;	

@Given("L'utilisateur est sur la page d'accueil")
public void l_utilisateur_est_sur_la_page_d_accueil() {
	System.setProperty("webdriver.gecko.driver", "./rsc/geckodriver.exe");
	driver = new FirefoxDriver();
	driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	driver.get("https://katalon-demo-cura.herokuapp.com/");
}

@When("L'utilisateur souhaite prendre un rendez-vous")
public void l_utilisateur_souhaite_prendre_un_rendez_vous() {
	driver.findElement(By.xpath("//a[@id=\"btn-make-appointment\"]")).click();
}

@Then("La page de connexion s'affiche")
public void la_page_de_connexion_s_affiche() {
	assertEquals(driver.findElement(By.xpath("//h2")).getText(),"Login");
}

@When("L'utilisateur se connecte")
public void l_utilisateur_se_connecte() {
	driver.findElement(By.xpath("//input[@name=\"username\"]")).clear();
	driver.findElement(By.xpath("//input[@name=\"username\"]")).sendKeys("John Doe");
	driver.findElement(By.xpath("//input[@name=\"password\"]")).clear();
	driver.findElement(By.xpath("//input[@name=\"password\"]")).sendKeys("ThisIsNotAPassword");
	driver.findElement(By.xpath("//button[@id=\"btn-login\"]")).click();
}

@Then("L'utilisateur est connecté sur la page de rendez-vous")
public void l_utilisateur_est_connecté_sur_la_page_de_rendez_vous() {
	assertEquals(driver.findElement(By.xpath("//h2")).getText(),"Make Appointment");
	driver.quit();
}
}